import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CustomerTest {
    private Customer customer;
    private Movie movie;
    private Movie movie2;
    private Movie movie3;
    private Rental rent;
    private Rental rent2;
    private Rental rent3;

    @Before
    public void setUp() {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
        movie2 = new Movie("Windi the Movie", Movie.NEW_RELEASE);
        rent2 = new Rental(movie2, 2);
        movie3 = new Movie("Windi the Movie 2", Movie.CHILDREN);
        rent3 = new Rental(movie3, 1);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 11"));
        assertTrue(result.contains("4 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void htmlStatementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 11"));
        assertTrue(result.contains("4 frequent renter points"));
    }


}