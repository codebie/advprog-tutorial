import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            double thisAmount = each.getAmount();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                    + String.valueOf(thisAmount) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "\n";
        result += "You earned " + String.valueOf(Rental.getFrequentRenterPoints(rentals))
                + " frequent renter points";

        return result;
    }

    public String htmlStatement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "<br>";

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            double thisAmount = each.getAmount();

            // Show figures for this rental
            result += "&nbsp;" + each.getMovie().getTitle() + "&nbsp;"
                    + String.valueOf(thisAmount) + "<br>";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "<br>";
        result += "You earned " + String.valueOf(Rental.getFrequentRenterPoints(rentals))
                + " frequent renter points";

        return result;
    }

    private double getTotalAmount() {
        Iterator<Rental> iterator = rentals.iterator();
        double result = 0;

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            result += each.getAmount();
        }

        return result;
    }
}