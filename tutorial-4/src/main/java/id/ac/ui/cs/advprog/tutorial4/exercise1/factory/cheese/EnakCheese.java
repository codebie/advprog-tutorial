package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class EnakCheese implements Cheese {

    public String toString() {
        return "Enak cheese";
    }
}
