package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class EnakClams implements Clams {

    public String toString() {
        return "Enak Clams from Depok Town Square";
    }
}
