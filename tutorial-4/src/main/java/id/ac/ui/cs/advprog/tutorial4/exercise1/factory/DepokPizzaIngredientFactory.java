package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.EnakCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.EnakClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.EnakDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.EnakSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Cabbage;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new EnakDough();
    }

    @Override
    public Sauce createSauce() {
        return new EnakSauce();
    }

    @Override
    public Cheese createCheese() {
        return new EnakCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Eggplant(), new Cabbage()};
        return veggies;
    }

    @Override
    public Clams createClam() {
        return new EnakClams();
    }
}
