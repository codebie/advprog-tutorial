package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class EnakDough implements Dough {
    public String toString() {
        return "Enak dough";
    }
}
