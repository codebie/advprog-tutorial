package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class EnakSauceTest {
    private EnakSauce enakSauce;

    @Before
    public void setUp() {
        enakSauce = new EnakSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Enak Sauce", enakSauce.toString());
    }
}
