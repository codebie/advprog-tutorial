package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class EnakDoughTest {
    private EnakDough enakDough;

    @Before
    public void setUp() {
        enakDough = new EnakDough();
    }

    @Test
    public void testToString() {
        assertEquals("Enak dough", enakDough.toString());
    }
}
