package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class EnakClamsTest {
    private EnakClams enakClams;

    @Before
    public void setUp() {
        enakClams = new EnakClams();
    }

    @Test
    public void testToString() {
        assertEquals("Enak Clams from Depok Town Square", enakClams.toString());
    }
}
