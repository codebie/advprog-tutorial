package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class CabbageTest {
    private Cabbage cabbage;

    @Before
    public void setUp() {
        cabbage = new Cabbage();
    }

    @Test
    public void testToString() {
        assertEquals("Cabbage", cabbage.toString());
    }
}
