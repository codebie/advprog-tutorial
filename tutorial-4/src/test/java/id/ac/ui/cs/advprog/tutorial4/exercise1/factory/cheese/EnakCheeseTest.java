package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class EnakCheeseTest {
    private EnakCheese enakCheese;

    @Before
    public void setUp() {
        enakCheese = new EnakCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Enak cheese", enakCheese.toString());
    }

}
